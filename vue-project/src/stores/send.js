import { ref } from "vue";
import { defineStore } from "pinia";

// 汇报模块选择发送人功能
export const useSendStore = defineStore("send", () => {
  const chosenData = ref([]);
  const reportData = ref({
    title: "",
    name: "",
    help: "",
    content: "",
  });

  // 存储选择的数据为一个数组
  function setChosenData(data) {
    if (Array.isArray(data)) {
      // 检查 data 是否为一个数组
      chosenData.value = data;
    } else {
      console.error("setChosenData expects an array");
    }
  }
  function setReportData(data) {
    if (Object.prototype.toString.call(data) === "[object Object]") {
      reportData.value = data;
    } else {
      console.error("addReportData expects an object");
    }
  }

  function reset() {
    chosenData.value = []; // 重置 chosenData 为初始值
    reportData.value = {
      title: "",
      name: "",
      help: "",
      content: "",
    };
  }
  return { chosenData, setChosenData, reset, reportData, setReportData };
});
