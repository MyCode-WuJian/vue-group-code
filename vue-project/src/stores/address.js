import { ref } from "vue";
import { defineStore } from "pinia";

// 通讯录模块存储被选中的tab页
export const useAddressStore = defineStore("chosen", () => {
  const chosenTab = ref(0);
  function setChosenTab(data) {
    chosenTab.value = data;
  }
  return { chosenTab, setChosenTab };
});