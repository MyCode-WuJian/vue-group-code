import { ref } from "vue";
import { defineStore } from "pinia";

export const useContractStore = defineStore('contract', () => {
    const myContractList = ref([]);
    function setMyContractList(obj) {
        obj.contractId = myContractList.value.length ? myContractList.value[myContractList.value.length - 1].contractId + 1 : 1;
        myContractList.value.push(obj);
    }
    return {
        myContractList,
        setMyContractList
    }
})