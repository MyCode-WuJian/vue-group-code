import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useRegisterStore = defineStore('counter', () => {
	const userData = ref([])
	const isLoading = ref(false)

	function setUserData(data) {
		if (isLoading.value) return
		isLoading.value = true

		// 尝试从localStorage读取数据  
		try {
			const savedData = JSON.parse(localStorage.getItem("data")) || [];
			console.log(savedData);
			savedData.push(data)
			localStorage.setItem('data', JSON.stringify(savedData))
			userData.value = savedData
				isLoading.value = false;
		} catch (e) {
			console.error("Error reading/writing data to localStorage:", e)
		}
	}

	return { userData, setUserData }
})