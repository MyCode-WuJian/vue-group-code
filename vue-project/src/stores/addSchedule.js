import { ref, onMounted,reactive } from "vue";
import { defineStore } from "pinia";

export const useScheduleInfo = defineStore("scheduleInfo", () => {
  const newScheduleInfo = ref([]);

  onMounted(() => {
    const savedData = localStorage.getItem("scheduleInfo");
    if (savedData) {
      newScheduleInfo.value = JSON.parse(savedData);
    }
  });

  function setScheduleInfo(data) {
    console.log("data", data);
    newScheduleInfo.value.push(data);
    // 获取 userByPwd 对象
    const userByPwd = JSON.parse(localStorage.getItem("userByPwd"));
    let dataList = JSON.parse(localStorage.getItem("data"));
    let user = dataList.find((item) => item.id === userByPwd.id);
    // console.log(user);
    user.children.push(data);
    console.log(user);
    dataList = [user, ...dataList];
    // console.log(dataList);
    // 数据去重
    const uniqueDataList = Array.from(
      new Set(dataList.map(JSON.stringify))
    ).map(JSON.parse);
    console.log(uniqueDataList);
    // 将 userByPwd 转换为可变的对象
    const mutableUserByPwd = reactive(userByPwd);

    // 将 scheduleInfo 添加到 children 数组中
    mutableUserByPwd.children.push(data);

    // 将修改后的 userByPwd 对象保存回本地存储中
    localStorage.setItem("userByPwd", JSON.stringify(mutableUserByPwd));
    localStorage.setItem("scheduleInfo", JSON.stringify(newScheduleInfo.value));
    localStorage.setItem("data", JSON.stringify(uniqueDataList));
  }

  return { newScheduleInfo, setScheduleInfo };
});
