import { ref } from 'vue'
import { defineStore } from 'pinia'
import { Logger } from 'sass'

export const useApplyforstore = defineStore('conten', () => {
	const userApply = ref([])
	function setApplyfor(obj) {
		userApply.value.push(obj)
	}

	//请假申请
	const applyData = ref({
		urgentValue: "",
		applicant: "",
		start: "",
		end: "",
		content: "",
		leaveDays: "",
		applicant: ""
	});
	function setApplyData(data) {
		if (Object.prototype.toString.call(data) === "[object Object]") {
			applyData.value = data;
			
		} else {
			console.error("addReportData expects an object");
		}
	}
	const applyForAskList = ref([])
	function setApplyForAskList(data) {
		applyData.id = applyForAskList.value.length ? applyForAskList.value[applyForAskList.value.length - 1].id + 1 : 1;
		applyForAskList.value.push(data);
	}



	//加班申请
	const apply_Over = ref({
		applyPerson: "",
		start: "",
		end: "",
		content: "",
		applyOver: "",
		value: ""
	})
	function setApplyOver(data) {
		if (Object.prototype.toString.call(data) === "[object Object]") {
			apply_Over.value = data;
			//   console.log(apply_Over.value);
		} else {
			console.error("addReportData expects an object");
		}
	}



		//报销申请
		const submitData = ref({
			urgentValue: "",
			start: "",
			end: "",
			content: "",
			leaveDays: "",
			applicant: ""
		});
		function setApplySubmit(data) {
			if (Object.prototype.toString.call(data) === "[object Object]") {
				submitData.value = data;
				
			} else {
				console.error("addReportData expects an object");
			}
		}
		const applyForSubmitList = ref([])
		function setapplyForSubmitList(data) {
			submitData.id = applyForSubmitList.value.length ? applyForSubmitList.value[applyForSubmitList.value.length - 1].id + 1 : 1;
			applyForSubmitList.value.push(data);
			console.log(applyForSubmitList);
		}

	return {
		userApply,
		setApplyfor,
		applyForAskList,
		setApplyForAskList,
		applyData,
		setApplyData,
		apply_Over,
		setApplyOver,
		submitData,
		setApplySubmit,
		applyForSubmitList,
		setapplyForSubmitList
	}
})