import { createApp } from "vue";
import { createPinia } from "pinia";
// import { Icon } from "vant";
import App from "./App.vue";
import router from "./router";

// 引入格式化工具
import "normalize.css/normalize.css";

import { piniaLasting } from 'pinia-lasting'
// Toast
import { showToast } from "vant";
import "vant/es/toast/style";

// Dialog
import { showDialog } from "vant";
import "vant/es/dialog/style";

// Notify
import { showNotify } from "vant";
import "vant/es/notify/style";

// ImagePreview
import { showImagePreview } from "vant";
import "vant/es/image-preview/style";

const app = createApp(App);

const pinia = createPinia()
pinia.use(piniaLasting)

app.use(createPinia());
app.use(router);
// app.use(Icon);

app.mount("#app");
