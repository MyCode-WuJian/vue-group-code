import { createRouter, createWebHashHistory } from "vue-router";
import workView from "../views/work/index.vue";

const routes = [
  {
    path: "/",
    name: "work",
    component: workView,
  },
  {
    path: "/schedule",
    name: "schedule",
    component: () => import("../views/schedule/index.vue"),
    children: [
      {
        path: "scheduledata",
        name: "scheduledata",
        component: () => import("../views/schedule/scheduledata.vue"),
      },
      {
        path: "addschedule",
        name: "addschedule",
        component: () => import("../views/schedule/addSchedule.vue"),
      },
      {
        path: "newschedule",
        name: "newschedule",
        component: () => import("../views/schedule/newScheduleInfo.vue"),
      },
      {
        path: "newscheduledetails",
        name: "newscheduledetails",
        component: () => import("@/views/schedule/newScheduleDetails.vue"),
      },
    ],
  },
  {
    path: "/address",
    name: "address",
    meta: {
      hidden: true,
    },
    component: () => import("../views/address/index.vue"),
    children: [
      {
        path: "addressList",
        name: "addressList",
        meta: {
          hidden: false,
        },
        component: () => import("../views/address/addressList.vue"),
      },
      {
        path: "addressUserList",
        name: "addressUserList",
        meta: {
          hidden: true,
        },
        component: () =>
          import("../views/address/addressUserList.vue"),
      },
    ],
  },

  {
    path: "/my",
    name: "my",
    meta: {
      hidden: true,
    },
    component: () => import("../views/my/index.vue"),
    children: [
      {
        // 我的页面
        path: "myMessage",
        name: "myMessage",
        meta: {
          hidden: false,
        },
        component: () => import("../views/my/myMessage.vue"),
      },
      {
        // 我的资料页面
        path: "myDetail",
        name: "myDetail",
        meta: {
          // 利用 路由元信息 帮助我们区分到底哪个页面需要展示 底部导航
          hidden: true,
        },
        component: () => import("../views/my/myDetail.vue"),
      },
      {
        // 加班申请
        path: "overStatistics",
        name: "overStatistics",
        meta: {
          // 利用 路由元信息 帮助我们区分到底哪个页面需要展示 底部导航
          hidden: true,
        },
        component: () => import("../views/my/overStatistics.vue"),
      },
      {
        // 请假申请
        path: "askForLeave",
        name: "askForLeave",
        meta: {
          // 利用 路由元信息 帮助我们区分到底哪个页面需要展示 底部导航
          hidden: true,
        },
        component: () => import("../views/my/askForLeave.vue"),
      },
      {
        // 报销申请
        path: "submit",
        name: "submit",
        meta: {
          // 利用 路由元信息 帮助我们区分到底哪个页面需要展示 底部导航
          hidden: true,
        },
        component: () => import("../views/my/submit.vue"),
      },
    ],
  },
  {
    path: "/report",
    name: "report",
    meta: {
      // 利用 路由元信息 帮助我们区分到底哪个页面需要展示 底部导航
      hidden: true,
    },
    component: () => import("../views/report/index.vue"),
    children: [
      {
        path: "reports",
        name: "reports",
        component: () => import("../views/report/reports.vue"),
      },
      {
        path: "addReport",
        name: "addReport",
        meta: {
          // 利用 路由元信息 帮助我们区分到底哪个页面需要展示 底部导航
          hidden: true,
        },
        component: () => import("../views/report/addReport.vue"),
      },
      {
        path: "reportDetail",
        name: "reportDetail",
        component: () => import("../views/report/reportDetail.vue"),
      },
      {
        path: "send",
        name: "send",
        component: () => import("../views/report/send.vue"),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    meta: {
      hidden: true,
    },
    component: () => import("../views/login/index.vue"),
    children: [
      {
        path: "loginByCode",
        name: "loginByCode",
        component: () => import("../views/login/loginByCode.vue"),
      },
      {
        path: "loginByPwd",
        name: "loginByPwd",
        component: () => import("../views/login/loginByPwd.vue"),
      },
      {
        path: "forgetPwd",
        name: "forgetPwd",
        component: () => import("../views/login/forgetPwd.vue"),
      },
    ],
  },
  {
    path: "/register",
    name: "register",
    meta: {
      hidden: true,
    },
    component: () => import("../views/register/index.vue"),
  },
  {
    path: "/applyfor",
    name: "applyfor",
    meta: {
      hidden: true,
    },
    component: () => import("../views/applyfor/index.vue"),
    children: [
      {
        path: "ApplyFor_ask",
        name: "ApplyFor_ask",
        component: () => import("../views/applyfor/ApplyFor_ask.vue"),
      },
      {
        path: "ApplyFor_beWorker",
        name: "ApplyFor_beWorker",
        component: () => import("../views/applyfor/ApplyFor_beWorker.vue"),
      },
      {
        path: "ApplyFor_overwork",
        name: "ApplyFor_overwork",
        component: () => import("../views/applyfor/ApplyFor_overwork.vue"),
      },
      {
        path: "ApplyFor_submit",
        name: "ApplyFor_submit",
        component: () => import("../views/applyfor/ApplyFor_submit.vue"),
      },
    ],
  },
  {
    path: "/contract",
    name: "contract",
    meta: {
      hidden: true,
    },
    component: () => import("../views/contract/index.vue"),
    children: [
      {
        path: "myContract",
        name: "myContract",
        component: () => import("../views/contract/myContract.vue"),
      },
      {
        path: "uploadContract",
        name: "uploadContract",
        component: () => import("../views/contract/uploadContract.vue"),
      },
      {
        path: "contractDetail",
        name: "contractDetail",
        component: () => import("../views/contract/contractDetail.vue"),
      },
      {
        path: "editContract",
        name: "editContract",
        component: () => import("../views/contract/editContract.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
});
router.beforeEach((to, from, next) => {
  const user = localStorage.getItem("userByPwd");
  const phoneNumUser = localStorage.getItem("phoneNum");

  if (
    (to.path == "/" ||
      to.path.startsWith("/schedule") ||
      to.path.startsWith("/my")) &&
    !user &&
    !phoneNumUser
  ) {
    next("/login/loginByPwd"); // 如果要跳转的路径是根路径并且本地存储没有user这个数据时，重定向到登录页
  } else {
    next(); // 其他情况放行
  }
});

export default router;
