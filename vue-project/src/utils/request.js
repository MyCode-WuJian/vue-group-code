import axios from "axios";

//引入办公首页的mock数据
import '@/mock/work'

// 引入登录注册的mock数据
import '@/mock/login.js'
import '@/mock/register.js'

// 引入申请模块的mock数据
import '@/mock/applyfor.js'



// 引入日程模块中查看日程的mock数据
import '@/mock/schedule.js'
import '@/mock/addSchedule.js'

// 引入汇报模块的mock数据
import '@/mock/report'
// 引入通讯录模块的mock数据
import '@/mock/address'

import '@/mock/contract'

const instance = axios.create({
    baseURL: "/",
    timeout: 60000,
});

instance.interceptors.request.use(
    function (config) {
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    function (response) {
        return response.data;
    },
    function (error) {
        return Promise.reject(error);
    }
);

export default function ajax(options) {
    const { url, method = "get", data = {} } = options;

    const reg = /^get$/i;

    if (reg.test(method)) {
        return instance.get(url, {
            params: data,
        });
    } else {
        return instance.post(url, data);
    }
}
