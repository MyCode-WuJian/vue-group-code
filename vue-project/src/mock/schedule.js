import Mock from "mockjs";
// const Mock = require('mockjs')

// 生成唯一 ID
const generateUniqueId = (() => {
  let id = 0;
  return () => {
    return id++;
  };
})();

// 随机部门属性
const departments = [
  "销售部",
  "市场部",
  "人力资源部",
  "财务部",
  "技术部",
  "客服部",
];

// 生成随机年份和月份的时间
let usedDates = new Set(); // Keep track of used dates

const generateRandomDate = () => {
  const now = new Date(); // Get current date
  const currentYear = now.getFullYear(); // Get current year

  let randomYear, randomMonth;
  do {
    randomYear = Mock.Random.integer(currentYear, currentYear + 1); // Generate random year within the next 5 years
    randomMonth = Mock.Random.integer(1, 12); // Generate random month
  } while (usedDates.has(`${randomYear}-${randomMonth}`)); // Check if the combination has been used

  usedDates.add(`${randomYear}-${randomMonth}`); // Add the combination to the set of used dates

  return `${randomYear}-${randomMonth}月份`;
};

// 生成模拟数据
const scheduleData = () => {
  const data = Mock.mock({
    "list|5-10": [
      {
        "id|+1": generateUniqueId(), // 生成唯一 ID
        "department|1": departments, // 随机部门属性
        time: generateRandomDate(), // 随机年份和月份的时间
        "isCompleted|1": true, // 随机布尔值
      },
    ],
    "newList|5-10": [
      {
        "id|+1": generateUniqueId(), // 生成唯一 ID
        "department|1": departments, // 随机部门属性
        'time': generateRandomDate(), // 随机年份和月份的时间
        "isCompleted|1": true, // 随机布尔值
      },
    ],
  });
  return {list:data.list,newList:data.newList};
};

// 利用mock 封装一个接口
Mock.mock("/api/scheduledata", "get", scheduleData);
