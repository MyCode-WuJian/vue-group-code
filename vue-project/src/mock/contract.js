import Mock from "mockjs";  
// 模拟一个发送验证码的 API  
Mock.mock("/api/uploadContract", "post", (config) => { 

  return {  
    code: "200",  
    message: "上传成功",  
    data: config 
  };  
});