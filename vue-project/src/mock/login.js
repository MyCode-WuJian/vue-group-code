import Mock from "mockjs";

// 模拟生成五位数的验证码  
const generateVerificationCode = () => {
	return Math.floor(10000 + Math.random() * 90000);
};

// 模拟一个发送验证码的 API  
Mock.mock("/api/sendVerificationCode", "post", (config) => {
	// 从请求参数中获取电话号码  
	const phoneNum = config.body.phoneNum;

	// 生成验证码并存储到模拟数据中  
	const verificationCode = generateVerificationCode();


	// 返回模拟的验证码响应给前端  
	return {
		status: "success",
		message: "验证码已发送",
		data: {
			verificationCode: verificationCode,
			phoneNum: phoneNum, // 这里添加了电话号码作为响应数据的一部分 
			children:[] 
		},
	};
});