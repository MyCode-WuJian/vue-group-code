import Mock from "mockjs";


Mock.mock("/api/addschedule", "post", (config) => {
 console.log(JSON.parse(config.body));
  // 从请求参数中获取电话号码
  const title = JSON.parse(config.body).title;
  const startTime = JSON.parse(config.body).startTime;
  const endTime = JSON.parse(config.body).endTime;
  const urgent = JSON.parse(config.body).urgent;
  const dateWarning = JSON.parse(config.body).dateWarning;
  const dateDesc = JSON.parse(config.body).dateDesc;


  // 返回模拟的验证码响应给前端
  return {
    status: "200",
    message: "添加任务成功",
    data: {
     title,
     startTime,
     endTime,
     urgent,
     dateWarning,
     dateDesc
    },
  };
});
