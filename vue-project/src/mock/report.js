import Mock from "mockjs";

// const data = Mock.mock({
//   // 属性 reports 的值是一个数组，其中含有 1 到 10 个元素
//   "reports|1-10": [
//     {
//       // 属性 id 是一个自增数，起始值为 1，每次增 1
//       "id|+1": 1,
//       // 随机生成中文名字
//       name: "@cname",
//       title: function () {
//         return this.name + "工作周报";
//       },
//       // 随机生成日期
//       date: "@date('yyyy年MM月dd日 HH:mm')",
//       // 随机生成中文段落作为汇报内容
//       content: "@cparagraph",
//       //
//       help:"@cparagraph",
//       //
//       send:[],
//       // 随机生成图片
//       // image: "@image('200x100', '#50B347', '#FFF', 'Mock.js')"
//     },
//   ],
// });
const data = Mock.mock({
  reports: [],
});

// 利用mock 封装一个接口
Mock.mock("/api/report/reports", "get", () => {
  return data.reports;
});

// 模拟一个获取详细信息的 API
Mock.mock(RegExp("/api/report/detail" + ".*"), "get", (config) => {
  // 获取请求路径中的 id
  const id = config.url.split("/").pop();
  // 从数据列表中找到 id 对应的数据
  const report = data.reports.find((item) => item.id === Number(id));
  // 返回 id 对应的数据
  return report;
});

// 模拟一个添加数据的 API
Mock.mock("/api/report/addReport", "post", (config) => {
  // 获取请求体中的数据
  const report = JSON.parse(config.body);
  // 为新数据分配一个 id
  report.id = data.reports.length
    ? data.reports[data.reports.length - 1].id + 1
    : 1;
  // 将新数据添加到数据列表中
  // console.log(data.reports);
  // console.log(report);
  data.reports.push(report);
  return {
    code: "200",
    message: "提交成功",
    data: {
      report
    },
  };
});
