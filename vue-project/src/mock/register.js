import Mock from "mockjs";  
// 模拟一个发送验证码的 API  
Mock.mock("/api/register", "post", (config) => { 
  // 从请求参数中获取电话号码 
  const phoneNum = JSON.parse(config.body).phoneNum;  
  const password = JSON.parse(config.body).password;  
  const nickname = JSON.parse(config.body).nickname;

  const id = Math.random().toString(36).substring(2); // 示例使用随机数生成ID 

  
  // 返回模拟的验证码响应给前端  
  return {  
    status: "200",  
    message: "注册成功",  
    data: {  
      id,
      password,  
      phoneNum, // 这里添加了电话号码作为响应数据的一部分  
      nickname,
      children:[]  // 用来存新建的日程
    },  
  };  
});