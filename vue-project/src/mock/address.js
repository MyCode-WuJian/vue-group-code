import Mock from "mockjs";

// const addressList = [
//   {
//     depId: 1,
//     depName: "研发部",
//     parentId: 0,
//     children: [
//       {
//         parentId: 1,
//         userId: 1,
//         userName: "张三",
//         userPhone: "13588888882",
//       },
//       {
//         parentId: 1,
//         userId: 2,
//         userName: "李四",
//         userPhone: "13688888884",
//       },
//       {
//         parentId: 1,
//         userId: 3,
//         userName: "王五",
//         userPhone: "13288888886",
//       },
//       {
//         parentId: 1,
//         userId: 4,
//         userName: "赵六",
//         userPhone: "15988888888",
//       },
//       {
//         parentId: 1,
//         userId: 5,
//         userName: "孙七",
//         userPhone: "16988888888",
//       },
//     ],
//   },
//   {
//     depId: 2,
//     depName: "市场部",
//     parentId: 0,
//     children: [
//       {
//         parentId: 2,
//         userId: 6,
//         userName: "周八",
//         userPhone: "13888888888",
//       },
//       {
//         parentId: 2,
//         userId: 7,
//         userName: "吴九",
//         userPhone: "13188888888",
//       },
//     ],
//   },
//   {
//     depId: 3,
//     depName: "财务部",
//     parentId: 0,
//     children: [
//       {
//         parentId: 3,
//         userId: 8,
//         userName: "郑十",
//         userPhone: "13588888888",
//       },
//       {
//         parentId: 3,
//         userId: 9,
//         userName: "钱十一",
//         userPhone: "13988888887",
//       },
//       {
//         parentId: 3,
//         userId: 10,
//         userName: "孔十二",
//         userPhone: "13588888886",
//       }
//     ],
//   },
// ];


const sourceList = [

  { depId: 1, depName: "研发部", parentId: 0 },
  { parentId: 1, userId: 1, userName: "张三", userPhone: "13588888882" },
  { parentId: 1, userId: 2, userName: "李四", userPhone: "13688888884" },
  { parentId: 1, userId: 3, userName: "王五", userPhone: "13288888886" },
  { parentId: 1, userId: 4, userName: "赵六", userPhone: "15988888888" },
  { parentId: 1, userId: 5, userName: "孙七", userPhone: "16988888888" },

  { depId: 2, depName: "市场部", parentId: 0 },
  { parentId: 2, userId: 6, userName: "周八", userPhone: "13888888888" },
  { parentId: 2, userId: 7, userName: "吴九", userPhone: "13188888888" },
  { parentId: 2, userId: 8, userName: "郑十", userPhone: "13588888888" },

  { depId: 3, depName: "财务部", parentId: 0 },
	{ parentId: 3, userId: 9, userName: "钱十一", userPhone: "13788888888" },
	{ parentId: 3, userId: 10, userName: "孔十二", userPhone: "13588888886" },
	{ parentId: 3, userId: 11, userName: "吴十三", userPhone: "13988888888" },
	{ parentId: 3, userId: 12, userName: "郑十四", userPhone: "13388888888" },

  { depId: 4, depName: "人力资源部", parentId: 0 },
  { parentId: 4, userId: 13, userName: "孙十五", userPhone: "13588888888" },
  { parentId: 4, userId: 14, userName: "钱十六", userPhone: "13588888886" },
  { parentId: 4, userId: 15, userName: "周十七", userPhone: "13588888888" },
  { parentId: 4, userId: 16, userName: "吴十八", userPhone: "13588888888" },
  { parentId: 4, userId: 17, userName: "郑十九", userPhone: "13588888888" },
  { parentId: 4, userId: 18, userName: "钱二十", userPhone: "13588888888" },

  { depId: 5, depName: "运营部", parentId: 0 },
  { parentId: 5, userId: 19, userName: "孙二十一", userPhone: "13588888888" },
  { parentId: 5, userId: 20, userName: "钱二十二", userPhone: "13588888886" },
  { parentId: 5, userId: 21, userName: "周二十三", userPhone: "13588888888" },
  { parentId: 5, userId: 22, userName: "吴二十四", userPhone: "13588888888" },
  { parentId: 5, userId: 23, userName: "郑二十五", userPhone: "13588888888" },
  { parentId: 5, userId: 24, userName: "钱二十六", userPhone: "13588888888" },
  { parentId: 5, userId: 25, userName: "孙二十七", userPhone: "13588888888" },
  { parentId: 5, userId: 26, userName: "钱二十八", userPhone: "13588888888" },
  
];


// 数组转树形结构
function getData(arr) {
	// 根据 parentId 进行过滤，找到父级节点
  return arr.filter((item) => {
	// 再根据 depId 进行过滤，找到子级节点，给父级节点添加 children 属性，值为子级节点
    item.children = arr.filter((child) => item.depId === child.parentId);
    return item.parentId === 0;
  });
}

// 利用mock 封装一个接口
let addressList = [];
Mock.mock("/api/address/addressList", "get", () => {
  addressList = getData(sourceList);
  return addressList;
});

// 根据用户id获取常用联系人列表
Mock.mock(/\/api\/address\/oftenList/, "get", (options) => {
  addressList = getData(sourceList);
  // 使用split 方法将 URL 分割成一个数组，然后取第 4 个元素（索引为 3）作为 id
  const id = options.url.split("/")[4];
  let result = null;
  addressList.forEach((item) => {
    if (result) return;
    item.children.forEach((child) => {
      if (child.userId === parseInt(id)) {
        addressList.find((item) => {
          if (item.depId === child.parentId) {
            result = item.children;
            //排除自己
            result = result.filter((item) => item.userId !== parseInt(id));
            return;
          }
        });
      }
    });
  });
  return result;
});

// 根据部门id获取用户信息 
Mock.mock(/\/api\/address\/userList/, "get", (options) => {
  addressList = getData(sourceList);
  const depId = options.url.split("/")[4];
  let list = null;
  addressList.forEach((item) => {
    if (list) return;
    if (item.depId === parseInt(depId)) {
      list = item.children;
    }
  });
  return list;
});





