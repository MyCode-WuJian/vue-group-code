import Mock from "mockjs";


const data = Mock.mock({
  reports: [],
});

// 利用mock 封装一个接口
// Mock.mock("/api/applyforAsk", "get", () => {
//   return data.reports;
// });

Mock.mock("/api/applyforAsk", "post", (config) => {
  // const report = JSON.parse(config.body)
  // console.log('请求体',report);
  // // 获取请求体中的数据
  // const urgentValue = JSON.parse(config.body).urgentValue;
  // const start = JSON.parse(config.body).start;
  // const end = JSON.parse(config.body).end;
  // const applyOver = JSON.parse(config.body).applyOver
  // const content = JSON.parse(config.body).content;
  // const leaveDays = JSON.parse(config.body).leaveDays;
  // const applicant = JSON.parse(config.body).applicant;
  // // 为新数据分配一个 id
  // report.id = 1
  // // 将新数据添加到数据列表中
  // // console.log(data.reports);
  // // console.log(report);
  // // data.reports.push(report);
  
  return {
    clod: "200",  
    message: "提交成功",    
    data: config
  };
});

Mock.mock("/api/applyforBeWorker", "post", (config) => {
  
  // 获取请求体中的数据
  const report = JSON.parse(config.body);

  const applyPerson = JSON.parse(config.body).applyPerson;
  const urgentValue = JSON.parse(config.body).urgentValue;
  const urgentValues = JSON.parse(config.body).urgentValues;
  const content = JSON.parse(config.body).content;
  const start = JSON.parse(config.body).start;
  // 为新数据分配一个 id
  report.id = 1

  return {
    code: "200",  
    message: "提交成功", 
    data: {
      applyPerson,
      urgentValue,
      urgentValues,
      content,
      start
    }   
  };
});


//加班申请
Mock.mock("/api/applyforOver", "post", (config) => {
  // 获取请求体中的数据
  const report = JSON.parse(config.body);

  const applyPerson = JSON.parse(config.body).applyPerson;
  const start = JSON.parse(config.body).start;
  const applyOver = JSON.parse(config.body).applyOver
  const end = JSON.parse(config.body).end;
  const content = JSON.parse(config.body).content;
  const value = JSON.parse(config.body).value;
  // 为新数据分配一个 id
  report.id = 1
  // 将新数据添加到数据列表中
  // console.log(data.reports);
  // console.log(report);
  // data.reports.push(report);
  return {
    code: "200",  
    message: "提交成功",
    data: {
      applyPerson,
      start,
      applyOver,
      end,
      content,
      value
    }       
  };
});



Mock.mock("/api/applyforSoumit", "post", (config) => {
  // const report = JSON.parse(config.body)
  // // 获取请求体中的数据
  // const urgentValue = JSON.parse(config.body).urgentValue;
  // const start = JSON.parse(config.body).start;
  // const end = JSON.parse(config.body).end;
  // const content = JSON.parse(config.body).content;
  // const leaveDays = JSON.parse(config.body).leaveDays;
  // const applicant = JSON.parse(config.body).applicant;
  // // 为新数据分配一个 id
  // report.id = 1
  // // 将新数据添加到数据列表中
  // // console.log(data.reports);
  // // console.log(report);
  // // data.reports.push(report);
  return {
    clod: "200",  
    message: "提交成功",   
    data: config 
    // data: {
    //   urgentValue,
    //   start,
    //   end,
    //   content,
    //   leaveDays,
    //   applicant
    // }
  };
});