import ajax from "@/utils/request";

// 添加日程
export function addSchedule(data) {
  return ajax({
    url: "/api/addschedule",
    method: "post",
    data,
  });
}
