import ajax from "@/utils/request";

// 获取组织机构树
export function getAddressList() {
  return ajax({
    url: "/api/address/addressList"
  });
}

// 获取常用联系人列表
export function getOftenList(id) {
  return ajax({
    url: "/api/address/oftenList/"+id
  });
}

// 获取通讯录好友详情
export function getUserList(depId,depName) {
  return ajax({
    url: `/api/address/userList/${depId}/${depName}`,
  });
}

