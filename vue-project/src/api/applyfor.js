import ajax from "@/utils/request";

//获取请假申请
export function goLoginByCode(data) {
    return ajax({
        url: '/api/applyforAsk',
        method: 'post',
        data
    })
}

//转正申请
export function goLoginByCode_beWorker(data) {
    return ajax({
        url: '/api/applyforBeWorker',
        method: 'post',
        data
    })
}

//加班申请
export function goLoginByCode_over(data) {
    return ajax({
        url: '/api/applyforOver',
        method: 'post',
        data
    })
}

//报销申请
export function goLoginByCode_submit(data) {
    return ajax({
        url: '/api/applyforSubmit',
        method: 'post',
        data
    })
}