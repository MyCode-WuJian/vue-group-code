import ajax from "@/utils/request";
export function gotoRegister(data){
    return ajax({
        url:'/api/register',
        method:'post',
        data
    })
}