import ajax from "@/utils/request";
export function goLoginByCode(data){
    return ajax({
        url:'/api/sendVerificationCode',
        method:'post',
        data
    })
}