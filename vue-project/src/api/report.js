import ajax from "@/utils/request";

// 获取汇报列表
export function getReportList() {
  return ajax({
    url: "/api/report/reports"
  });
}

// 获取汇报详情
export function getReportDetail(id) {
  return ajax({
    url: `/api/report/detail/${id}`,
  });
}

// 添加汇报
export function addReport(data){
  return ajax({
    url:'/api/report/addReport',
    method:'POST',
    data
  })
}