import ajax from "@/utils/request";
export function uploadContract(data){
    return ajax({
        url:'/api/uploadContract',
        method:'post',
        data
    })
}